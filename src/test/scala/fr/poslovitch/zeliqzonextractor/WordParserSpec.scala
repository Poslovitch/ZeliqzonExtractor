package fr.poslovitch.zeliqzonextractor

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class WordParserSpec extends AnyFlatSpec with Matchers {

  // SPECIFIC UNIT TESTS -- TESTS SPECIFIC PARTS OF AN ENTRY, TO MAKE SURE PARSING IS CORRECT AND HANDLES EDGE CASES

  "WordParser WrittenRep" should "parse the writtenRep of a basic entry" in {
    val input = "Coron [''kǫrõ M''], s. m. — Bout de fil."
    val parsedEntry = WordParser.parse(WordParser.wordEntry, input).get
    parsedEntry.writtenRep shouldEqual "Coron"
  }

  it should "parse the writtenRep of a redirection" in {
    val input = "Cossenou, voir {{DPRML|Cosson}}."
    val parsedEntry = WordParser.parse(WordParser.wordEntry, input).get
    parsedEntry.writtenRep shouldEqual "Cossenou"
  }

  "WordParser Pronunciation" should "parse a pronunciation containing one pronunciation and one location" in {
    val input = "kǫrõ M"
    val pronunciation = WordParser.parse(WordParser.pronunciationComponent, input).get
    pronunciation.pronunciation shouldEqual "kǫrõ"
    pronunciation.places shouldEqual Some(List("M"))
  }

  it should "parse a pronunciation containing one pronunciation and a few locations" in {
    val input = "ętǫrdī(r) M, I, P, N"
    val pronunciation = WordParser.parse(WordParser.pronunciationComponent, input).get
    pronunciation.pronunciation shouldEqual "ętǫrdī(r)"
    pronunciation.places shouldEqual Some(List("M", "I", "P", "N"))
  }

  it should "parse a pronunciation containing one pronunciation and location gén." in {
    val input = "ętōt gén."
    val pronunciation = WordParser.parse(WordParser.pronunciationComponent, input).get
    pronunciation.pronunciation shouldEqual "ętōt"
    pronunciation.places shouldEqual Some(List("gén."))
  }

  it should "parse a pronunciation containing one pronunciation and a town location" in {
    val input = "jœt Gondrexange"
    val pronunciation = WordParser.parse(WordParser.pronunciationComponent, input).get
    pronunciation.pronunciation shouldEqual "jœt"
    pronunciation.places shouldEqual Some(List("Gondrexange"))
  }

  it should "parse a pronunciation containing one pronunciation" in {
    val input = "[''jǟl M'']"
    val pronunciation = WordParser.parse(WordParser.pronunciation, input).get
    pronunciation.size shouldBe 1
    pronunciation.head.pronunciation shouldEqual "jǟl"
    pronunciation.head.places shouldEqual Some(List("M"))
  }

  it should "parse a pronunciation containing two pronunciations" in {
    val input = "[''bawę̄y-bǫwę̄y M, N, bǫwę̄y I, P'']"
    val pronunciation = WordParser.parse(WordParser.pronunciation, input).get
    pronunciation.size shouldBe 2

    val pron1 = pronunciation.head
    pron1.pronunciation shouldEqual "bawę̄y-bǫwę̄y"
    pron1.places shouldEqual Some(List("M", "N"))

    val pron2 = pronunciation.tail.head
    pron2.pronunciation shouldEqual "bǫwę̄y"
    pron2.places shouldEqual Some(List("I", "P"))
  }

  it should "not fail when parsing pronunciation with dots" in {
    val input = "[''kǫrtina M, kortinę P, F..'']"
  }

  //TODO: How to handle things like : "Cortinat, Cortinèt [''kǫrtina M, kortinę P, F..''], adj. — Courtaud." ?

  // "COMPLETE" UNIT TESTS -- TESTS A WHOLE ENTRY TO MAKE SURE EVERYTHING IS WORKING AS EXPECTED

  "WordParser" should "parse a basic entry with a single word" in {
    val input = "Èrgautine [''ęrgōtin M, I, P''], s. f. — Pré où les oies vont pâturer."
    val parsedWords = WordParser.parse(input)

    parsedWords.size shouldBe 1 // there should only be one word here

    val ergautine = parsedWords.head

    ergautine.writtenRep shouldEqual "Èrgautine" // We should have a writtenRep
    ergautine.senses shouldEqual Some
    ergautine.pronunciation shouldEqual Some
    ergautine.seeAlso shouldEqual None // And a see also

    // Senses
    val senses = ergautine.senses.get
    senses.size shouldBe 1
  }

  it should "parse a redirection" in {
    val input = "Ācer, voir {{DPRML|Ācieu}}."
    val parsedWords = WordParser.parse(input)

    parsedWords.size shouldBe 1 // there should only be one word here

    val acer = parsedWords.head

    acer.writtenRep shouldEqual "Ācer" // We should have a writtenRep
    acer.seeAlso shouldEqual Some // And a see also
    // But no senses nor pronunciation
    acer.senses shouldEqual None
    acer.pronunciation shouldEqual None
  }

  it should "parse an entry with two senses" in {
    val input = "Acacac [''äkäkäk M''], interj. — {{1o}} Cri de douleur quand on se brûle. {{2o}} Cri d’éloignement pour détourner un enfant d’une vilaine chose."
    val parsedWords = WordParser.parse(input)

    parsedWords.size shouldBe 1 // there should only be one word here

    val acacac = parsedWords.head

    acacac.writtenRep shouldEqual "Acacac"
    acacac.pronunciation shouldBe a [Some[Seq[WordPronunciation]]]
    acacac.senses shouldBe Some
    acacac.seeAlso shouldBe None

    // Senses
    val senses = acacac.senses.get
    senses.size shouldBe 2
  }
}

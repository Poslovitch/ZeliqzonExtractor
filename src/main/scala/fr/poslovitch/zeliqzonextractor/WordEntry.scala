package fr.poslovitch.zeliqzonextractor

case class WordEntry(writtenRep: String,
                     pronunciation: Option[Seq[WordPronunciation]],
                     senses: Option[Seq[WordSense]],
                     seeAlso: Option[Seq[String]])

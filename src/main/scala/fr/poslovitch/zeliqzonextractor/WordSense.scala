package fr.poslovitch.zeliqzonextractor

case class WordSense(definition: String,
                     grammaticalCategory: Option[GrammaticalCategory],
                     examples: Option[Seq[WordExample]],
                     seeAlso: Option[Seq[String]])

package fr.poslovitch.zeliqzonextractor

case class WordExample(example: String,
                       translation: Option[String])

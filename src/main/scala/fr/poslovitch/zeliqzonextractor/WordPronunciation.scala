package fr.poslovitch.zeliqzonextractor

case class WordPronunciation(pronunciation: String,
                             places: Option[Seq[String]])

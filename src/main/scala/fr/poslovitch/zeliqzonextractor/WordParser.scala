package fr.poslovitch.zeliqzonextractor

import scala.util.parsing.combinator.RegexParsers

object WordParser extends RegexParsers {

  private def space: Parser[Any] = ' '
  private def comma: Parser[Any] = ','
  private def cadratin: Parser[Any] = '—'

  /**
   * Catches the writtenRep of the word at the beginning of the line.
   * @return the writtenRep of the word at the beginning of the line.
   */
  def writtenRep: Parser[String] = "^[^ ,]+".r

  def pronunciationLocation: Parser[String] = "[IFMNSPV]|gén\\.|[A-Z][^ ]*".r
  def pronunciationComponent: Parser[WordPronunciation] = "[^ ]*".r ~ opt(space ~> repsep(pronunciationLocation, ", ")) ^^ {
    result => WordPronunciation(result._1, result._2)
  }
  def pronunciation: Parser[Seq[WordPronunciation]] = "\\[''".r ~> rep1sep(pronunciationComponent, space | comma | ", ".r) <~ "'{0,2}]".r

  def wordEntry: Parser[WordEntry] = writtenRep ~ opt((space ~> pronunciation) | (comma ~> pronunciation)) ^^ { result =>
    val writtenRep~pronunciation = result
    WordEntry(writtenRep = writtenRep,
      seeAlso = Option.empty,
      senses = Option.empty,
      pronunciation = if (pronunciation.isDefined) Option(pronunciation.get) else Option.empty)
  }

  def parse(str: String): Seq[WordEntry] = Seq(parse(wordEntry, str).get)
}

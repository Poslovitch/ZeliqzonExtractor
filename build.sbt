import Dependencies._

lazy val root = (project in file("."))
  .settings(
    name := "ZeliqzonExtractor",
    description := "todo",
    homepage := Some(url("https://codeberg.org/Poslovitch/ZeliqzonExtractor")),
    scmInfo := Some(
      ScmInfo(
        url("https://codeberg.org/Poslovitch/ZeliqzonExtractor"),
        "scm:git@codeberg.org:Poslovitch/ZeliqzonExtractor.git"
      )
    ),
    libraryDependencies ++= Seq(
      scalaTest % Test,
      scalaParserCombinators,
      akkaActor,
      akkaStream,
      akkaSlf4J
    )
  )

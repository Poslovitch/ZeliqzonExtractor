import sbt._

object Version {
  lazy val scalaVersion = "2.13.8"
  lazy val scalaTest = "3.2.12"
  lazy val scalaParserCombinators = "1.1.2"
  lazy val akkaStream = "2.6.19"
  lazy val akkaActor = "2.6.19"
  lazy val akkaSlf4J = "2.6.19"
}

object Dependencies {
  lazy val scalaTest = "org.scalatest" %% "scalatest" % Version.scalaTest
  lazy val scalaParserCombinators = "org.scala-lang.modules" %% "scala-parser-combinators" % Version.scalaParserCombinators

  lazy val akkaActor = "com.typesafe.akka" %% "akka-actor" % Version.akkaActor
  lazy val akkaStream = "com.typesafe.akka" %% "akka-stream" % Version.akkaStream
  lazy val akkaSlf4J = "com.typesafe.akka" %% "akka-slf4j" % Version.akkaSlf4J
}